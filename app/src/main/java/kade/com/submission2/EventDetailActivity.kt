package kade.com.submission2

import android.Manifest
import android.content.Context
import android.graphics.Typeface
import android.net.ConnectivityManager
import android.os.Bundle
import android.support.annotation.RequiresPermission
import android.support.v7.app.AppCompatActivity
import android.view.Gravity
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.ProgressBar
import com.bumptech.glide.Glide
import com.google.gson.Gson
import kade.com.submission2.data.Team
import kade.com.submission2.presenter.EventDetailPresenter
import kade.com.submission2.utils.ApiRepo
import kade.com.submission2.utils.invisible
import kade.com.submission2.utils.visible
import org.jetbrains.anko.*

class EventDetailActivity :  AppCompatActivity(),EventDetailUI {

    private lateinit var presenter: EventDetailPresenter

    private lateinit var teamAwayId:String
    private lateinit var teamHomeId:String
    private lateinit var teamAwayName:String
    private lateinit var teamHomeName:String
    private lateinit var teamAwayScore:String
    private lateinit var teamHomeScore:String

    private lateinit var teamHomeGoalKeeper:String
    private lateinit var teamHomeDefense:String
    private lateinit var teamHomeMidField:String
    private lateinit var teamHomeForward:String
    private lateinit var teamHomeSubstitutes:String

    private lateinit var teamAwayGoalKeeper:String
    private lateinit var teamAwayDefense:String
    private lateinit var teamAwayMidField:String
    private lateinit var teamAwayForward:String
    private lateinit var teamAwaySubstitutes:String

    private lateinit var eventName:String
    private lateinit var eventDate:String
    private lateinit var teamAwayImg : ImageView
    private lateinit var teamHomeImg: ImageView

    private lateinit var progressBar: ProgressBar

    private fun teamSplitter(data:String?) : String {
        var dataSplit = data.toString().split(";")
        var textStr:String = ""
//        if (dataSplit.size>1){
            for (x in 0..(dataSplit.size-2)){
                textStr += dataSplit[x] +"\n"
            }
//        }
        return textStr
    }
    private fun dateformat(data: String?) : String {
        var dataInput : String = data.toString()
        var arrData = dataInput.split("-")
        var bulan : String?
        when(arrData[1]){
            "01"-> bulan = "Jan"
            "02"-> bulan = "Feb"
            "03"-> bulan = "Mar"
            "04"-> bulan = "Apr"
            "05"-> bulan = "Mei"
            "06"-> bulan = "Jun"
            "07"-> bulan = "Jul"
            "08"-> bulan = "Aug"
            "09"-> bulan = "Sep"
            "10"-> bulan = "Oct"
            "11"-> bulan = "Nov"
            "12"-> bulan = "Dec"
            else->{
                bulan = "-"
            }
        }
        return arrData[2]+" "+bulan+" "+arrData[0]
    }
    @RequiresPermission(value = Manifest.permission.ACCESS_NETWORK_STATE)
    fun Context.isConnected(): Boolean {
        val connectivityManager = this
                .getSystemService(Context.CONNECTIVITY_SERVICE) as? ConnectivityManager
        connectivityManager?.let {
            val netInfo = it.activeNetworkInfo
            netInfo?.let {
                if (it.isConnected) return true
            }
        }
        return false
    }
    private fun initData(){
        eventName = intent.getStringExtra("eventName")
        eventDate = dateformat(intent.getStringExtra("eventDate"))

        teamAwayId = intent.getStringExtra("teamAwayId")
        teamHomeId = intent.getStringExtra("teamHomeId")
        teamAwayName = intent.getStringExtra("teamAwayName")
        teamHomeName = intent.getStringExtra("teamHomeName")

        if(intent.getStringExtra("teamHomeScore").isNullOrBlank()) teamHomeScore = "-" else teamHomeScore =intent.getStringExtra("teamHomeScore")
        if(intent.getStringExtra("teamAwayScore").isNullOrBlank()) teamAwayScore = "-" else teamAwayScore = intent.getStringExtra("teamAwayScore")

        if(intent.getStringExtra("teamHomeGoalKeeper").isNullOrBlank()) teamHomeGoalKeeper = "-" else teamHomeGoalKeeper = teamSplitter(intent.getStringExtra("teamHomeGoalKeeper"))
        if(intent.getStringExtra("teamHomeDefense").isNullOrBlank()) teamHomeDefense = "-" else teamHomeDefense = teamSplitter(intent.getStringExtra("teamHomeDefense"))
        if(intent.getStringExtra("teamHomeMidField").isNullOrBlank()) teamHomeMidField = "-" else teamHomeMidField = teamSplitter(intent.getStringExtra("teamHomeMidField"))
        if(intent.getStringExtra("teamHomeForward").isNullOrBlank()) teamHomeForward = "-" else teamHomeForward = teamSplitter(intent.getStringExtra("teamHomeForward"))
        if(intent.getStringExtra("teamHomeSubstitutes").isNullOrBlank()) teamHomeSubstitutes = "-" else teamHomeSubstitutes =teamSplitter(intent.getStringExtra("teamHomeSubstitutes"))

        if(intent.getStringExtra("teamAwayGoalKeeper").isNullOrBlank()) teamAwayGoalKeeper = "-" else {teamAwayGoalKeeper = teamSplitter(intent.getStringExtra("teamAwayGoalKeeper"))}
        if(intent.getStringExtra("teamAwayDefense").isNullOrBlank()) teamAwayDefense = "-" else {teamAwayDefense = teamSplitter(intent.getStringExtra("teamAwayDefense")) }
        if(intent.getStringExtra("teamAwayMidField").isNullOrBlank()) teamAwayMidField = "-" else {teamAwayMidField = teamSplitter(intent.getStringExtra("teamAwayMidField")) }
        if(intent.getStringExtra("teamAwayForward").isNullOrBlank()) teamAwayForward = "-" else {teamAwayForward = teamSplitter(intent.getStringExtra("teamAwayForward")) }
        if(intent.getStringExtra("teamAwaySubstitutes").isNullOrBlank()) teamAwaySubstitutes = "-" else {teamAwaySubstitutes =teamSplitter(intent.getStringExtra("teamAwaySubstitutes")) }
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        var data:Intent = intent
        initData()
        scrollView {
            lparams(matchParent, wrapContent)

            linearLayout{
                lparams(matchParent, wrapContent)
                orientation = LinearLayout.VERTICAL
                linearLayout {
                    lparams(matchParent, wrapContent){
                        leftPadding = dip(12)
                        rightPadding = dip(12)
                    }
                    orientation = LinearLayout.VERTICAL
                    progressBar = progressBar {
                    }.lparams{
                        gravity = Gravity.CENTER_HORIZONTAL
                        bottomPadding = dip(12); topPadding = dip(12)
                    }
                    textView { // Date Event
                        text = eventDate
                        textSize = 24f
                        textAlignment = View.TEXT_ALIGNMENT_CENTER
                    }.lparams(matchParent, wrapContent){
                        bottomPadding = dip(5); topPadding = dip(5)
                    }.setTypeface(Typeface.DEFAULT_BOLD)

                    linearLayout {
                        orientation = LinearLayout.HORIZONTAL
                        linearLayout {
                            //TEAM HOME
                            textView {
                                textSize = 20f
                                text = teamHomeName
                                textAlignment = View.TEXT_ALIGNMENT_TEXT_START
                            }.lparams(matchParent, wrapContent){
                                bottomPadding = dip(8)
                            }.setTypeface(Typeface.DEFAULT_BOLD)
                        }.lparams(0, wrapContent){
                            weight=1f
                        }

                        linearLayout {
                            //TEAM AWAY
                            textView {
                                textSize = 20f
                                text = teamAwayName
                                textAlignment = View.TEXT_ALIGNMENT_TEXT_END
                            }.lparams(matchParent, wrapContent){
                                bottomPadding = dip(8)
                            }.setTypeface(Typeface.DEFAULT_BOLD)
                        }.lparams(0, wrapContent){
                            weight=1f
                        }
                    }.lparams (matchParent, wrapContent)
                    linearLayout {
                        lparams(matchParent, wrapContent)
                        orientation = LinearLayout.HORIZONTAL
                        //TEAM HOME
                        linearLayout {
                            orientation = LinearLayout.VERTICAL

                            teamHomeImg = imageView { // Team Home
//                                layoutParams = LinearLayout.LayoutParams(dip(64),dip(64))
                            }.lparams(dip(64),dip(64)){
                                gravity = Gravity.CENTER_HORIZONTAL
                            }
                            textView {
                                text = teamHomeScore
                                textSize = 24f
                            }.lparams{
                                gravity = Gravity.CENTER_HORIZONTAL
                            }.setTypeface(Typeface.DEFAULT_BOLD)
                        }.lparams(0, wrapContent){
                            weight = 2f
                        }

                        //VS
                        linearLayout {
                            textView {
                                text = "VS"
                                textSize = 16f
                                textAlignment = View.TEXT_ALIGNMENT_CENTER
                                gravity = Gravity.CENTER
                            }.lparams(matchParent, matchParent)
                        }.lparams(0, matchParent){
                            weight = 1f
                        }

                        //TEAM AWAY
                        linearLayout {
                            orientation = LinearLayout.VERTICAL
                            teamAwayImg = imageView { // Team Away
//                                layoutParams = LinearLayout.LayoutParams(dip(64),dip(64))
                            }.lparams(dip(64),dip(64)){
                                gravity = Gravity.CENTER_HORIZONTAL
                            }
                            textView {
                                text = teamAwayScore
                                textSize = 24f
                            }.lparams{
                                gravity = Gravity.CENTER_HORIZONTAL
                            }.setTypeface(Typeface.DEFAULT_BOLD)
                        }.lparams(0, wrapContent){
                            weight = 2f
                        }
                    } // HORIZONTAL

                    //GoalKeeper
                    textView {
                        text = "Goal Keeper"
                        textSize = 18f
                        textAlignment = View.TEXT_ALIGNMENT_CENTER
                    }.lparams(matchParent, wrapContent).setTypeface(Typeface.DEFAULT_BOLD)
                    linearLayout {
                        orientation = LinearLayout.HORIZONTAL
                        textView {
                            text = teamAwayGoalKeeper
                            textAlignment = View.TEXT_ALIGNMENT_TEXT_START
                        }.lparams(0, wrapContent){
                            weight = 1f
                        }
                        textView {
                            text = teamHomeGoalKeeper
                            textAlignment = View.TEXT_ALIGNMENT_TEXT_END
                        }.lparams(0, wrapContent){
                            weight = 1f
                        }
                    }.lparams(matchParent, wrapContent)

                    //Defense
                    textView {
                        text = "Defense"
                        textSize = 18f
                        textAlignment = View.TEXT_ALIGNMENT_CENTER
                    }.lparams(matchParent, wrapContent).setTypeface(Typeface.DEFAULT_BOLD)
                    linearLayout {
                        orientation = LinearLayout.HORIZONTAL
                        textView {
                            text = teamAwayDefense
                            textAlignment = View.TEXT_ALIGNMENT_TEXT_START
                        }.lparams(0, wrapContent){
                            weight = 1f
                        }
                        textView {
                            text = teamHomeDefense
                            textAlignment = View.TEXT_ALIGNMENT_TEXT_END
                        }.lparams(0, wrapContent){
                            weight = 1f
                        }
                    }.lparams(matchParent, wrapContent)

                    //MidField
                    textView {
                        text = "MidField"
                        textSize = 18f
                        textAlignment = View.TEXT_ALIGNMENT_CENTER
                    }.lparams(matchParent, wrapContent).setTypeface(Typeface.DEFAULT_BOLD)
                    linearLayout {
                        orientation = LinearLayout.HORIZONTAL
                        textView {
                            text = teamAwayMidField
                            textAlignment = View.TEXT_ALIGNMENT_TEXT_START
                        }.lparams(0, wrapContent){
                            weight = 1f
                        }
                        textView {
                            text = teamHomeMidField
                            textAlignment = View.TEXT_ALIGNMENT_TEXT_END
                        }.lparams(0, wrapContent){
                            weight = 1f
                        }
                    }.lparams(matchParent, wrapContent)

                    //Forward
                    textView {
                        text = "Forward"
                        textSize = 18f
                        textAlignment = View.TEXT_ALIGNMENT_CENTER
                    }.lparams(matchParent, wrapContent).setTypeface(Typeface.DEFAULT_BOLD)
                    linearLayout {
                        orientation = LinearLayout.HORIZONTAL
                        textView {
                            text = teamAwayForward
                            textAlignment = View.TEXT_ALIGNMENT_TEXT_START
                        }.lparams(0, wrapContent){
                            weight = 1f
                        }
                        textView {
                            text = teamHomeForward
                            textAlignment = View.TEXT_ALIGNMENT_TEXT_END
                        }.lparams(0, wrapContent){
                            weight = 1f
                        }
                    }.lparams(matchParent, wrapContent)

                    //Substitutes
                    textView {
                        text = "Substitutes"
                        textSize = 18f
                        textAlignment = View.TEXT_ALIGNMENT_CENTER
                    }.lparams(matchParent, wrapContent).setTypeface(Typeface.DEFAULT_BOLD)
                    linearLayout {
                        orientation = LinearLayout.HORIZONTAL
                        textView {
                            text = teamAwaySubstitutes
                            textAlignment = View.TEXT_ALIGNMENT_TEXT_START
                        }.lparams(0, wrapContent){
                            weight = 1f
                        }
                        textView {
                            text = teamHomeSubstitutes
                            textAlignment = View.TEXT_ALIGNMENT_TEXT_END
                        }.lparams(0, wrapContent){
                            weight = 1f
                        }
                    }.lparams(matchParent, wrapContent)

                } //VERTIKAL
            }
        } // SCROLL VIEW
        val request = ApiRepo()
        val gson = Gson()
        presenter = EventDetailPresenter(this, request, gson)
        if (isConnected()){
            presenter.getTeamAway(intent.getStringExtra("teamAwayId"))
            presenter.getTeamHome(intent.getStringExtra("teamHomeId"))
        }

    }
    override fun showTeamAway(awayTeam: List<Team>) {
        Glide.with(getBaseContext()).load(awayTeam.get(0).teamBadge).into(teamHomeImg)
    }

    override fun showTeamHome(homeTeam: List<Team>) {
        Glide.with(getBaseContext()).load(homeTeam.get(0).teamBadge ).into(teamAwayImg )
    }

    override fun showLoading() {
        progressBar.visible()
    }

    override fun hideLoading() {
        progressBar.invisible()
    }


}