package kade.com.submission2

import kade.com.submission2.data.Team

interface EventDetailUI {
    fun showTeamAway(awayTeam: List<Team>)
    fun showTeamHome(homeTeam: List<Team>)
    fun showLoading()
    fun hideLoading()
}