package kade.com.submission2

import kade.com.submission2.data.EventModel

interface MainUI {
    fun showLoading()
    fun hideLoading()
    fun updateEventMatch(data: List<EventModel>)
}