package kade.com.submission2.presenter

import android.util.Log
import com.google.gson.Gson
import kade.com.submission2.utils.ApiRepo
import kade.com.submission2.EventDetailUI
import kade.com.submission2.utils.TSDBApi
import kade.com.submission2.data.TeamResponse
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

class EventDetailPresenter(private val view: EventDetailUI,
                           private val apiRepository: ApiRepo,
                           private val gson: Gson) {
    fun getTeamAway(teamAwayId: String?) {
        view.showLoading()
        doAsync {
            var awayTeam = gson.fromJson(apiRepository
                    .doRequest(TSDBApi.getTeams(teamAwayId)),
                    TeamResponse::class.java)

            uiThread {
                view.hideLoading()
                view.showTeamAway(awayTeam.teams)
            }
        }
    }
    fun getTeamHome(teamHomeId: String?) {
        view.showLoading()
        doAsync {

            var homeTeam = gson.fromJson(apiRepository
                    .doRequest(TSDBApi.getTeams(teamHomeId)),
                    TeamResponse::class.java)

            uiThread {
                view.hideLoading()
                view.showTeamHome(homeTeam.teams)
            }
        }
    }
}