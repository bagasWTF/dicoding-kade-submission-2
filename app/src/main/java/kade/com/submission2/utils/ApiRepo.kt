package kade.com.submission2.utils

import java.net.URL

class ApiRepo {
    fun doRequest(url: String): String {
        return URL(url).readText()
    }
}